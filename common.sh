#!/bin/sh -e

die() {
  stderr "$@"
  false
}

stderr() {
  printf "%s\n" "$@" >&2
}

unsafe_stderr() {
  printf -- "$*\n" >&2
}

DEBUG() {
  [ "${DEBUG:-}" ] && unsafe_stderr "$@"
  true
}

load_vlandef_vars() {
  vlan_path="${1}"
  unset gateway4
  unset subnet4
  unset gateway
  unset subnet
  unset access
  unset reserved
  #DEBUG "  load ${vlan_path}"
  # TODO check that is a file and not a directory
  if [ -f "${vlan_path}" ]; then
    . "${vlan_path}"
  else
    stderr "ERROR: ${vlan_path} is not a file"
    [ "${EXIT}" ] && return 1
  fi
  gateway4="${gateway4:-}"
  subnet4="${subnet4:-}"
  gateway="${gateway:-}"
  subnet="${subnet:-}"
  access="${access:-}"
  reserved="${reserved:-}"
  if [ -z "${gateway4}" ] && [ -z "${gateway}" ] && [ "${access}" ]; then
    stderr "ERROR: ${vlan_path} exists but does not define gateway nor gateway4 and access var is defined"
    [ "${EXIT}" ] && return 1
  fi
  if [ -z "${subnet4}" ] && [ -z "${subnet}" ] && [ -z "${reserved}" ]; then
    # TODO "and reserved var is not defined"
    stderr "ERROR: ${vlan_path} exists but does not define subnet nor subnet4"
    [ "${EXIT}" ] && return 1
  fi
  true
}

load_vlan_host_var() {
  host_path="${1}"
  unset ip4
  unset ip
  #DEBUG "  load ${host_path}"
  if [ -f "${host_path}" ]; then
    . "${host_path}"
  else
    stderr "ERROR: ${host_path} is not a file"
    [ "${EXIT}" ] && return 1
  fi
  ip4="${ip4:-}"
  ip="${ip:-}"
  if [ -z "${ip4}" ] && [ -z "${ip}" ]; then
    stderr "ERROR: ${host_path} exists but does not define ip nor ip4"
    [ "${EXIT}" ] && return 1
  fi
  true
}
