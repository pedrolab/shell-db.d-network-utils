## requirements

```
sudo apt install sipcalc bc
```

## policy

situation that wants tracking is when hosts uses official vlans (for all vlans, check all vlans on hosts), then we expect to perform some checks

when hosts get non-official vlans and other stuff, then, it is not tracked and that's intended

## expected features

- [x] IPv4: see percentage remaining IPs and percentage of use (%)
- [x] avoid IP collision/duplication
  - [x] IPv4
  - [x] IPv6
- [x] verify subnetting
  - [x] IPv4
  - [x] IPv6
- [x] validation of object types
  - [x] IPv4 address (no CIDR): host (ip4), gateway (gateway4)
  - [x] IPv4 network (subnet)
  - [x] IPv6 address (no CIDR): host (ip), gateway (gateway)
  - [x] IPv6 network (subnet)
- [ ] other objects
  - [ ] an object type for static routes
- [ ] next IP on vlan/subnet
  - [x] IPv4 address
  - [ ] IPv6 address: maybe we should not care avoid having holes on the subnet (too much IPs, too complex to calculate the holes)
- [ ] sync data with a read-only netbox

## current schema

- vlans (dir)
  - vlanN (file)
    - type (var)
      - nondefined
      - access: then gateway and subnet together are mandatory
    - subnet (var)
    - ip (var)
    - gateway (var)
- host (dir)
  - cfg.env (file)
    - vm='y' (var): internal resource that is a virtual server
    - hypervisor='y' (var): internal resource that is an hypervisor (hosts vms)
    - vps='y' (var): external resource, if is a virtual private server (VPS), hence, you care about a third-party vm
    - dedicated='y' (var): custom allocated server (CAS), same as VPS for dedicated servers
  - vlanN (file)
    - ip (var)
    - future: subnet (var)

### future

- reserved (presence): when no subnet on vlan
- access (absence): when no gateway on vlan

## current limitations

- host/vlanN is currently mapped 1:1 to vlan/vlanN
  - hence, a host cannot assign two IPs on same vlan
  - n:1 could be easily implemented if we accept syntax for csv or allowing multiple values on one
- a floating IP around multiple hosts (VRRP)

## test framework

it uses shellspec

```
make test
```

- [ ] cover pool_status.sh
- [ ] separate ipv6 tests from ipv4 tests
