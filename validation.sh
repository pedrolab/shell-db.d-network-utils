#!/bin/sh -eu

#set -x

. ./common.sh

DB_NAME="${DB_NAME:-db.d}"
# retrieve VLANS_PATH and HOSTS_PATH vars
if [ -f "${DB_NAME}/cfg.env" ]; then
  . "${DB_NAME}/cfg.env"
fi
VLANS_PATH="${VLANS_PATH:-db.d/vlans}"
HOSTS_PATH="${HOSTS_PATH:-db.d/hosts}"

grep_sip() {
  local pattern="${1}"
  grep "${pattern}" | cut -d'-' -f2 | xargs
}

is_addr_in_subnet() {
  local addr="${1}"
  # subnet arg is the start of range (verified)
  local subnet="${2}"
  subnet_sipcalc="$(sipcalc "${subnet}")"

  if echo "${addr}${subnet}" | grep -q ':'; then
    ip_version='IPv6'
    grep_prefix="Prefix length"
    grep_net="Subnet prefix"
  else
    ip_version='IPv4'
    grep_prefix="Network mask (bits)"
    grep_net="Network address"
  fi
  subnet_prefix="$(echo "${subnet_sipcalc}" | grep_sip "${grep_prefix}")"
  DEBUG "subnet_prefix ${subnet_prefix}"
  subnet_net="$(echo "${subnet_sipcalc}" | grep_sip "${grep_net}")"
  DEBUG "subnet_net ${subnet_net}"
  DEBUG "addr/subnet_prefix ${addr}/${subnet_prefix}"
  addr_sipcalc="$(sipcalc "${addr}/${subnet_prefix}")"
  addr_net="$(echo "${addr_sipcalc}" | grep_sip "${grep_net}")"
  DEBUG "addr_net ${addr_net}"
  DEBUG "\n__is_addr_in_subnet() iteration\n  ${subnet_net} - ${addr_net}\n"
  if [ ! "${subnet_net}" = "${addr_net}" ]; then
    stderr "ERROR: ${ip_version} ${addr} is not part of subnet '${subnet}'"
    [ "${EXIT}" ] && return 1
  fi
  true
}

ipv6_addr_validation() {
  local addr="${1}"
  # clean sipcalc output
  addr_sipcalc="$(sipcalc "${addr}" | grep_sip "Compressed")"
  # conditions
  #   1. not detected by sipcalc, hence, not a valid ipv6
  #   2. detected the cidr notation, not allowed
  if [ -z "${addr_sipcalc}" ] || echo "${addr}" | grep -q '/'; then
    stderr "ERROR: ipv6 '${addr}' has invalid format, cidr is invalid too"
    [ "${EXIT}" ] && return 1
  fi
  true
}

ipv6_subnet_validation() {
  local subnet="${1}"
  subnet_sipcalc="$(sipcalc "${subnet}")"
  subnet_ea="$(echo "${subnet_sipcalc}" | grep_sip "Expanded Address")"
  if [ -z "${subnet_ea}" ]; then
    stderr "ERROR: ipv6 subnet '${subnet}' has invalid format"
    [ "${EXIT}" ] && return 1
  fi
  subnet_nr="$(echo "${subnet_sipcalc}" | grep_sip "Network range")"
  subnet_prefix="$(echo "${subnet_sipcalc}" | grep_sip "Prefix length")"
  subnet_nr_sipcalc="$(sipcalc "${subnet_nr}/${subnet_prefix}")"
  DEBUG "${subnet_nr}"
  subnet_comp="$(echo "${subnet_nr_sipcalc}" | grep_sip "Compressed")"
  if [ "${subnet_ea}" = "${subnet_nr}" ]; then
    true
  else
    stderr "ERROR: ipv6 subnet ${subnet} has invalid format, it should be ${subnet_comp}/${subnet_prefix}"
    [ "${EXIT}" ] && return 1
  fi
}

ipv4_subnet_validation() {
  local subnet="${1}"
  subnet_sipcalc="$(sipcalc "${subnet}")"
  subnet_na="$(echo "${subnet_sipcalc}" | grep_sip "Network address")"
  if [ -z "${subnet_na}" ]; then
    stderr "ERROR: ipv4 subnet '${subnet}' has invalid format"
    [ "${EXIT}" ] && return 1
  fi
  subnet_prefix="$(echo "${subnet_sipcalc}" | grep_sip "Network mask (bits)")"
  subnet_target="${subnet_na}/${subnet_prefix}"
  if [ "${subnet}" = "${subnet_target}" ]; then
    true
  else
    stderr "ERROR: ipv4 subnet ${subnet} has invalid format, it should be ${subnet_target}"
    [ "${EXIT}" ] && return 1
  fi
}

# thanks https://stackoverflow.com/questions/13777387/check-for-ip-validity/13778973#13778973
ipv4_addr_validation() {
  local ip4="${1}"
  if expr "$ip4" : '[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$' >/dev/null; then
    for i in 1 2 3 4; do
      if [ $(echo "$ip4" | cut -d. -f$i) -gt 255 ]; then
        stderr "ERROR: ipv4 ${ip4} has a number greater than 255"
        [ "${EXIT}" ] && return 1
      fi
    done
    return 0
  else
    stderr "ERROR: ipv4 '${ip4}' has invalid format, cidr is invalid too"
    [ "${EXIT}" ] && return 1
  fi
}

# validates:
#   - gateway4 and subnet4 exist
#   - gateway4 is in subnet subnet4
vlan_validation() {
  local vlan_path="${1}"
  vlan="$(basename ${vlan_path})"
  DEBUG "\n__vlan_validation() iteration\n  vlan: ${vlan}\n"
  load_vlandef_vars "${vlan_path}" || {
     [ "${EXIT}" ] && return 1
  }
  # check vlan schema is valid
  if [ "${gateway4}" ]; then
    ipv4_addr_validation "${gateway4}" || {
      stderr "file: ${vlan_path}"
      [ "${EXIT}" ] && return 1
    }
    ipv4_subnet_validation "${subnet4}" || {
      stderr "file: ${vlan_path}"
      [ "${EXIT}" ] && return 1
    }
    # TODO add file too
    is_addr_in_subnet "${gateway4}" "${subnet4}" || {
      [ "${EXIT}" ] && return 1
    }
  fi
  if [ "${gateway}" ]; then
    ipv6_addr_validation "${gateway}" || {
      stderr "file: ${vlan_path}"
      [ "${EXIT}" ] && return 1
    }
    ipv6_subnet_validation "${subnet}" || {
      stderr "file: ${vlan_path}"
      [ "${EXIT}" ] && return 1
    }
    # TODO add file too
    is_addr_in_subnet "${gateway}" "${subnet}" || {
      [ "${EXIT}" ] && return 1
    }
  fi
}

all_vlan_validation() {
  vlans="$(find "${VLANS_PATH}" -type f)"
  for vlan_path in ${vlans}; do
    vlan_validation "${vlan_path}"
  done
}

# validates:
#   - gateway4 and subnet4 exist
#   - ip:
#     - is in subnet subnet4
#     - ipv4 or ipv6 format
#   - duplicated address on ip and ip4 vars
vlan_on_host_validation() {
  local host_path="${1}"
  addrs="${2:-}"

  vlan="$(basename ${host_path})"
  host="$(basename $(dirname ${host_path}))"
  DEBUG "\n    __vlan_on_hosts_validation() host iteration\n    host: ${host}\n"
  load_vlan_host_var "${host_path}" || {
    [ "${EXIT}" ] && return 1
  }
  if [ "${ip4}" ]; then
    ipv4_addr_validation "${ip4}" || {
      stderr "file: ${host_path}"
      [ "${EXIT}" ] && return 1
    }
    load_vlandef_vars "${VLANS_PATH}/${vlan}" || {
      [ "${EXIT}" ] && return 1
    }
    # TODO add file too
    is_addr_in_subnet "${ip4}" "${subnet4}" || {
      [ "${EXIT}" ] && return 1
    }
    addrs="${ip4}\n${addrs}"
  fi
  if [ "${ip}" ]; then
    ipv6_addr_validation "${ip}" || {
      stderr "file: ${host_path}"
      [ "${EXIT}" ] && return 1
    }
    # TODO add file too
    is_addr_in_subnet "${ip}" "${subnet}" || {
      [ "${EXIT}" ] && return 1
    }
    addrs="${ip}\n${addrs}"
  fi

  echo "${addrs}"
}

vlan_on_hosts_validation() {
  vlans=$(find "${VLANS_PATH}" -type f)
  for vlan_path in ${vlans}; do
    vlan="$(basename ${vlan_path})"
    DEBUG "\n__vlan_on_hosts_validation() vlan iteration\n  vlan: ${vlan}\n"
    load_vlandef_vars "${vlan_path}"
    hosts="$(find "${HOSTS_PATH}" -iname "${vlan}" -type f)"
    addrs=''
    for host_path in ${hosts}; do
      addrs="$(vlan_on_host_validation "${host_path}" "${addrs}")"
    done
    duplicated="$(echo "${addrs}" | uniq -d)"
    if [ "${duplicated}" ]; then
      unsafe_stderr "ERROR: on ${vlan} there is a duplicated address:\n${duplicated}\naffected files:"
      grep -lR "${duplicated}" "${DB_NAME}"
      [ "${EXIT}" ] && return 1
    fi
  done
}
