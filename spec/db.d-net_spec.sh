EXIT='y'
Describe 'validation.sh'
  DB_NAME='test-db.d'
  Include ./validation.sh

  Describe 'vlan_validation()'
    It 'success because vlan is fine'
      When call vlan_validation "${DB_NAME}/vlans/vlan95"
      The status should be success
    End
    It 'fails because misses gateway var'
      target_file="${DB_NAME}/invalid_vlans/vlan95_nogw_v4"
      When call vlan_validation "${target_file}"
      The status should be failure
      The stderr should equal "ERROR: ${target_file} exists but does not define gateway nor gateway4 and access var is defined"
    End
    It 'fails because misses network var'
      target_file="${DB_NAME}/invalid_vlans/vlan95_nogw_v4_but_gw_v6"
      When call vlan_validation "${target_file}"
      The status should be failure
      The stderr should equal "ERROR: ${target_file} exists but does not define subnet nor subnet4"
    End
    It 'fails because misses network var'
      target_file="${DB_NAME}/invalid_vlans/vlan95_nonet_v4"
      When call vlan_validation "${target_file}"
      The status should be failure
      The stderr should equal "ERROR: ${target_file} exists but does not define subnet nor subnet4"
    End
    It 'fails because vlan file does not exist'
      target_file="${DB_NAME}/invalid_vlans/nonexist"
      When call vlan_validation "${target_file}"
      The status should be failure
      The stderr should equal "ERROR: ${target_file} is not a file"
    End
    It 'fails because gw addr is out of vlan network (ipv4)'
      target_file="${DB_NAME}/invalid_vlans/vlan95_gw_not_in_net_ipv4"
      When call vlan_validation "${target_file}"
      The status should be failure
      The stderr should equal "ERROR: IPv4 192.168.94.34 is not part of subnet '192.168.95.0/24'"
    End
    It 'fails because gw addr is out of vlan network (ipv6)'
      target_file="${DB_NAME}/invalid_vlans/vlan95_gw_not_in_net_ipv6"
      When call vlan_validation "${target_file}"
      The status should be failure
      The stderr should equal "ERROR: IPv6 2001:db8:2:2::1 is not part of subnet '2001:db8:1::/56'"
    End
    It 'fails because net addr has invalid format (ipv4)'
      target_file="${DB_NAME}/invalid_vlans/vlan95_invalid_subnet_ipv4"
      test_err_msg="$(cat <<END
ERROR: ipv4 subnet 192.168.1.1/24 has invalid format, it should be 192.168.1.0/24
file: ${target_file}
END
)"
      When call vlan_validation "${target_file}"
      The status should be failure
      The stderr should equal "${test_err_msg}"
    End
    It 'fails because net addr has invalid format (ipv6)'
      target_file="${DB_NAME}/invalid_vlans/vlan95_invalid_subnet_ipv6"
      test_err_msg="$(cat <<END
ERROR: ipv6 subnet '2001:db8:1:2/64' has invalid format
file: ${target_file}
END
)"
      When call vlan_validation "${target_file}"
      The status should be failure
      The stderr should equal "${test_err_msg}"
    End
    It 'fails because gw addr has invalid ipv4 format'
      target_file="${DB_NAME}/invalid_vlans/vlan95_invalidgw_ipv4"
      test_err_msg="$(cat <<END
ERROR: ipv4 ' 192.168.95.34 ' has invalid format, cidr is invalid too
file: ${target_file}
END
)"
      When call vlan_validation "${target_file}"
      The status should be failure
      The stderr should equal "${test_err_msg}"
    End
    It 'fails because gw addr has invalid ipv6 format'
      target_file="${DB_NAME}/invalid_vlans/vlan95_gw_invalid_ipv6"
      test_err_msg="$(cat <<END
ERROR: ipv6 '2001:db8:1:2' has invalid format, cidr is invalid too
file: ${target_file}
END
)"
      When call vlan_validation "${target_file}"
      The status should be failure
      The stderr should equal "${test_err_msg}"
    End
  End

  Describe 'vlan_on_host_validation()'
    It 'success because vlan on host is fine'
      target_file="${DB_NAME}/hosts/host1/vlan95"
      When call vlan_on_host_validation "${target_file}"
      The status should be success
      The stdout should equal '192.168.95.1'
    End
    It 'fails because because it misses addr var'
      target_file="${DB_NAME}/invalid_hosts/host1/vlan95_novar"
      When call vlan_on_host_validation "${target_file}"
      The status should be failure
      The stderr should equal "ERROR: ${target_file} exists but does not define ip nor ip4"
    End
    It 'fails because host addr is out of vlan network (ipv4)'
      target_file="${DB_NAME}/invalid_hosts/host1/vlan95"
      When call vlan_on_host_validation "${target_file}"
      The status should be failure
      The stderr should equal "ERROR: IPv4 1.1.1.1 is not part of subnet '192.168.95.0/24'"
    End
    It 'fails because addr has invalid ipv4 format'
      target_file="${DB_NAME}/invalid_hosts/host1/vlan95_invalidformat_ipv4"
      test_err_msg="$(cat <<END
ERROR: ipv4 ' 192.168.1.1' has invalid format, cidr is invalid too
file: ${target_file}
END
)"
      When call vlan_on_host_validation "${target_file}"
      The status should be failure
      The stderr should equal "${test_err_msg}"
    End
    It 'fails because addr has invalid ipv4 format'
      target_file="${DB_NAME}/invalid_hosts/host1/vlan95_invalidformat2"
      test_err_msg="$(cat <<END
ERROR: ipv4 192.168.1.1000 has a number greater than 255
file: ${target_file}
END
)"
      When call vlan_on_host_validation "${target_file}"
      The status should be failure
      The stderr should equal "${test_err_msg}"
    End
    It 'fails because addr has invalid ipv4 format (cidr format)'
      target_file="${DB_NAME}/invalid_hosts/host1/vlan95_invalidformat_ipv4_cidr"
      test_err_msg="$(cat <<END
ERROR: ipv4 '192.168.1.1/24' has invalid format, cidr is invalid too
file: ${target_file}
END
)"
      When call vlan_on_host_validation "${target_file}"
      The status should be failure
      The stderr should equal "${test_err_msg}"
    End
    It 'fails because addr has invalid ipv6 format (cidr format)'
      target_file="${DB_NAME}/invalid_hosts/host1/vlan95_invalidformat_ipv6_cidr"
      test_err_msg="$(cat <<END
ERROR: ipv6 '2001:db8::1/64' has invalid format, cidr is invalid too
file: ${target_file}
END
)"
      When call vlan_on_host_validation "${target_file}"
      The status should be failure
      The stderr should equal "${test_err_msg}"
    End
  End

End

# TODO
#Describe 'pool_status.sh'
