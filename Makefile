test:
	./test-with-shellcheck.sh
	# we ensure that temba is run in TEST mode
	shellspec --env TEST=true
