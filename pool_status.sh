#!/bin/sh -e

#set -x

. ./common.sh

calc_pool_ipv4() {
  echo "pool_ipv4: $(echo "${pool_ipv4}" | wc -l)"
}

print_pool_ipv4() {
  local vlan="${1}"
  load_vlandef_vars "db.d/vlans/${vlan}" || return 1
  # process sipcalc output
  #   1. get third column
  #   2. quit "header"
  #   3. detect ip
  #   4. trim
  pool_ipv4="$(sipcalc -s /32 "${subnet4}" \
    | cut -d'-' -f3 \
    | tail -n+4 \
    | grep "^[[:space:]]*[0-9].*" \
    | xargs printf "%s\n")"
  DEBUG "$(calc_pool_ipv4) - total"
  # substract network identity (first ip) and broadcast ip (last ip) -> src https://stackoverflow.com/questions/15856733/what-is-the-easiest-way-to-remove-1st-and-last-line-from-file-with-awk/33161138#33161138
  pool_ipv4="$(echo "${pool_ipv4}" | head -n -1 | tail -n+2)"
  DEBUG "$(calc_pool_ipv4) - quitting broadcast and network id"

  hosts="$(find db.d/hosts -iname "${vlan}" -type f)"
  # retrieve all addresses
  for host_path in ${hosts}; do
    load_vlan_host_var "${host_path}"
    if [ "${ip4}" ]; then
      prev_pool_ipv4="${pool_ipv4:-}"
      pool_ipv4="$(echo "${pool_ipv4}" | grep -v "${ip4}$")"
      # sanity check
      if [ "${prev_pool_ipv4}" = "${pool_ipv4}" ]; then
        stderr "${host_path}: WARNING: consistency error: ${ip4} not substracted from pool"
        stderr "=============="
        stderr "${pool_ipv4}"
        stderr "=============="
        return 1
      fi
    fi
    DEBUG "$(calc_pool_ipv4) - after host ${ip4}"
  done
  DEBUG "$(calc_pool_ipv4) - at the end of print_pool_ipv4"
  echo "${pool_ipv4}"
}

vlan_usage_ipv4() {
  local vlan="${1}"
  avail_ipv4_list="$(print_pool_ipv4 "${vlan}")"
  avail_ipv4="$(echo "${avail_ipv4_list}" | wc -l)"
  # take cidr subpart
  subnet4_prefix="$(echo "${subnet4}" | cut -d'/' -f2)"
  total_ips_pool_ipv4="$((1<<32-subnet4_prefix))"
  # thanks https://unix.stackexchange.com/questions/175744/bash-limiting-precision-of-floating-point-variables/175755#175755
  usage_rate="$(echo "scale=2; ${avail_ipv4}/${total_ips_pool_ipv4}*100" | bc -l)"
  echo "  Avail IPv4: ${avail_ipv4} (${usage_rate}%) "
}

total_vlan_usage_ipv4() {
  # only search on access vlans
  vlans="$(grep -Ril "^access=" "${VLANS_PATH}")"
  for vlan_path in ${vlans}; do
    vlan="$(basename ${vlan_path})"
    DEBUG "\n__total_vlan_usage_ipv4() iteration\n  vlan: ${vlan}\n"
    #load_vlandef_vars "db.d/vlans/${vlan}"
    load_vlandef_vars "${vlan_path}"
    if [ -z "${subnet4}" ]; then
      #echo "${vlan}: WARNING: no subnet definition"
      true
    else
      echo "${vlan} ${subnet4}"
      vlan_usage_ipv4 "${vlan}"
    fi
  done
}

next_ip_on_vlan_ipv4() {
  local vlan="${1}"
  load_vlandef_vars "db.d/vlans/${vlan}"
  next="$(print_pool_ipv4 "${vlan}" | head -1)"
  echo "next IPv4 on ${vlan} ${subnet4} is ${next}"
}
